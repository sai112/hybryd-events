#!/bin/bash

#docker compose --file dockerfiles/docker-compose.test.resize.yaml run --rm --build --volume $(pwd)/assets/testing/test_images:/data_in/test_images test_image_resizer

#docker compose --file dockerfiles/docker-compose.test.spectrogram.yaml run --rm --build --volume $(pwd)/assets/testing/test_audio:/data_in/test_audio test_spectrogram_builder

#docker compose --file dockerfiles/docker-compose.test.mobilenetv2.image.yaml run --rm --build test_mobilenetv2_image

#docker compose --file dockerfiles/docker-compose.test.mobilenetv2.audio.yaml run --rm --build test_mobilenetv2_audio

#docker compose --file dockerfiles/docker-compose.test.inference.yaml run --rm --build test_inference

#docker compose --file dockerfiles/docker-compose.test.resnet50v1.image.yaml run --rm --build test_resnet50v1_image

#docker compose --file dockerfiles/docker-compose.test.mobilenetv2.audio.dcase.type.yaml run --rm --build test_mobilenetv2_audio_dcase_type

#docker compose --file dockerfiles/docker-compose.test.mobilenetv2.audio.dcase.anomaly.yaml run --rm --build test_mobilenetv2_audio_dcase_anomaly

#docker compose --file dockerfiles/docker-compose.test.video_synth.type.yaml run --rm --build test_video_synth_type

#docker compose --file dockerfiles/docker-compose.test.generate_text.yaml run --rm --build test_generate_text

docker compose --file dockerfiles/docker-compose.test.video.type_detection.yaml run --rm --build test_video_type_detection

#docker compose --file dockerfiles/docker-compose.test.augmentation.audio.yaml run --rm --build test_augmentation_audio