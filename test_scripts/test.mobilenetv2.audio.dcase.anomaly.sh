#!/bin/bash

logs=/logs/
batch=20
steps=40000

SECONDS=0

python /src/prepare_training_infrastructure.py --content_root /content_fan --classes normal anomaly --batch $batch --steps $steps > $logs/fan_prepare_training_infrastructure.log 2>&1

ELAPSED_PREP="Prep: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"
SECONDS=0

python /src/models/research/object_detection/model_main_tf2.py --pipeline_config_path=/content_fan/training/pipeline.config --model_dir=/content_fan/train_dir --alsologtostderr > $logs/fan_train.log 2>&1

ELAPSED_TRAIN="Train: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"

python /src/models/research/object_detection/exporter_main_v2.py --input_type image_tensor --pipeline_config_path /content_fan/training/pipeline.config --trained_checkpoint_dir /content_fan/train_dir --output_directory /content_fan/saved_model_dir > $logs/fan_export.log 2>&1

python /src/models/research/object_detection/export_tflite_graph_tf2.py --pipeline_config_path /content_fan/saved_model_dir/pipeline.config --trained_checkpoint_dir /content_fan/saved_model_dir/checkpoint --output_directory /content_fan/tflite > $logs/fan_export_tflite.log 2>&1
tflite_convert --saved_model_dir=/content_fan/tflite/saved_model --output_file=/content_fan/tflite/model.tflite > $logs/fan_save_tflite.log 2>&1

echo $ELAPSED_PREP
echo $ELAPSED_TRAIN

echo "EXPECTED:"
echo "SAVED MODEL:"
echo "/content_fan/saved_model_dir:"
echo "pipeline.config  saved_model  checkpoint"

echo "/content_fan/saved_model_dir/saved_model:"
echo "saved_model.pb  fingerprint.pb  assets  variables"

echo "/content_fan/saved_model_dir/saved_model/assets:"

echo "/content_fan/saved_model_dir/saved_model/variables:"
echo "variables.data-00000-of-00001  variables.index"

echo "/content_fan/saved_model_dir/checkpoint:"
echo "checkpoint  ckpt-0.index  ckpt-0.data-00000-of-00001"
echo "EXPORT:"
echo "/content_fan/tflite/model.tflite"
echo ""
echo "ACTUAL:"
echo "SAVED MODEL:"
ls -tR /content_fan/saved_model_dir
echo "EXPORT:"
ls -tR /content_fan/tflite/*.*

python /src/model_evaluator.py --model_path /content_fan/tflite/model.tflite --label_map /content_fan/training/object_detection.pbtxt --test_samples /content_fan/training/test --target_width 320 --target_height 320  > $logs/fan_evaluation.log 2>&1

echo "==============================================================="

SECONDS=0

python /src/prepare_training_infrastructure.py --content_root /content_valve --classes normal anomaly --batch $batch --steps $steps > $logs/valve_prepare_training_infrastructure.log 2>&1

ELAPSED_PREP="Prep: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"
SECONDS=0

python /src/models/research/object_detection/model_main_tf2.py --pipeline_config_path=/content_valve/training/pipeline.config --model_dir=/content_valve/train_dir --alsologtostderr > $logs/valve_train.log 2>&1

ELAPSED_TRAIN="Train: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"

python /src/models/research/object_detection/exporter_main_v2.py --input_type image_tensor --pipeline_config_path /content_valve/training/pipeline.config --trained_checkpoint_dir /content_valve/train_dir --output_directory /content_valve/saved_model_dir > $logs/valve_export.log 2>&1

python /src/models/research/object_detection/export_tflite_graph_tf2.py --pipeline_config_path /content_valve/saved_model_dir/pipeline.config --trained_checkpoint_dir /content_valve/saved_model_dir/checkpoint --output_directory /content_valve/tflite > $logs/valve_export_tflite.log 2>&1
tflite_convert --saved_model_dir=/content_valve/tflite/saved_model --output_file=/content_valve/tflite/model.tflite > $logs/valve_save_tflite.log 2>&1

echo $ELAPSED_PREP
echo $ELAPSED_TRAIN

echo "EXPECTED:"
echo "SAVED MODEL:"
echo "/content_valve/saved_model_dir:"
echo "pipeline.config  saved_model  checkpoint"

echo "/content_valve/saved_model_dir/saved_model:"
echo "saved_model.pb  fingerprint.pb  assets  variables"

echo "/content_valve/saved_model_dir/saved_model/assets:"

echo "/content_valve/saved_model_dir/saved_model/variables:"
echo "variables.data-00000-of-00001  variables.index"

echo "/content_valve/saved_model_dir/checkpoint:"
echo "checkpoint  ckpt-0.index  ckpt-0.data-00000-of-00001"
echo "EXPORT:"
echo "/content_valve/tflite/model.tflite"
echo ""
echo "ACTUAL:"
echo "SAVED MODEL:"
ls -tR /content_valve/saved_model_dir
echo "EXPORT:"
ls -tR /content_valve/tflite/*.*

python /src/model_evaluator.py --model_path /content_valve/tflite/model.tflite --label_map /content_valve/training/object_detection.pbtxt --test_samples /content_valve/training/test --target_width 320 --target_height 320  > $logs/valve_evaluation.log 2>&1