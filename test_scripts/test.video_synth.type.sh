#!/bin/bash

perl /src/video_synth.type.pl /content/data_img/train/d1 /content/data_audio/fan /content/data_img/train/d8 /content/data_audio/valve /content/video_maps 10 fan valve /content/data_video

echo "EXPECTED:"
echo "Files generated:"
echo "70"
echo ""
echo "ACTUAL:"
echo "Files generated:"
find /content/video_maps -type f | wc -l