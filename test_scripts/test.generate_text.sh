#!/bin/bash

echo "EXPECTED:"
echo
echo "В работе устройства valve замечено долгосрочное аномальное поведение в середине и конце работы"
echo "В работе устройства valve замечено долгосрочное аномальное поведение в середине и конце работы"
echo "В работе устройства valve замечено долгосрочное аномальное поведение в началe и середине и конце работы"
echo "В работе устройства valve замечено краткосрочное аномальное поведение в началe работы"
echo "В работе устройства valve замечено долгосрочное аномальное поведение в середине и началe и конце работы"
echo
echo "ACTUAL:"
echo
python3 /src/output_parsing/parsing.py --Directory /src/output_parsing/output_files
python3 /src/output_parsing/parsing.py --device valve --mode 0 0 0 0 1 1 1 0 0
python3 /src/output_parsing/parsing.py --device valve --mode 1 1 1 1 1 1 1 1 1
python3 /src/output_parsing/parsing.py --device valve --mode 1 0 0 0 0 0 0 0 0
python3 /src/output_parsing/parsing.py --device valve --mode 1 1 1 1 1 1 1 0 0