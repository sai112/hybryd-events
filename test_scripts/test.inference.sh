#!/bin/bash

echo "EXPECTED:"
echo ""
echo "ACTUAL:"

python /src/inference_file_processor.py --model_path /content/tflite/model.tflite --source_dir /content/data/audio --target_width 320 --target_height 320

python /src/inference_file_processor.py --model_path /content/tflite/model.tflite --source_dir /content/data/images --target_width 320 --target_height 320