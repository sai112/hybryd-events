#!/bin/bash

#perl /src/video_synth.type.pl /content/data_img/train/d1 /content/data_audio/fan /content/data_img/train/d8 /content/data_audio/valve /content/video_maps 10 fan valve /content/data_video

echo "EXPECTED:"
echo "Files generated:"
echo "70"
echo ""
echo "ACTUAL:"
echo "Files generated:"

python /src/model_evaluator_continious_audio.py --model_path /content/models/model_dcase_2c_50k.tflite --label_map /content/object_detection.pbtxt --test_samples /content/audio_samples --target_width 320 --target_height 320 --im_storage /tmp_aud