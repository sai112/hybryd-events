#!/bin/bash

python3 /src/resize_images.py --source /data_in/test_images --target /data_out/test_images --width 320 --height 320

echo "EXPECTED:"
echo "/data_out/test_images/28x28png.png  /data_out/test_images/600x600jpg.jpg"
echo "/data_out/test_images/28x28png.png: PNG image data, 320 x 320, 1-bit colormap, non-interlaced"
echo "/data_out/test_images/600x600jpg.jpg: JPEG image data, JFIF standard 1.01, aspect ratio, density 1x1, segment length 16, baseline, precision 8, 320x320, components 3"
echo ""
echo "ACTUAL:"
ls -tR /data_out/**/*.*
file /data_out/test_images/28x28png.png
file /data_out/test_images/600x600jpg.jpg