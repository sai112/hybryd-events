#!/bin/bash

SECONDS=0

python /src/prepare_training_infrastructure.py --classes fan valve --batch 20 --steps 50000

ELAPSED_PREP="Prep: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"
SECONDS=0

python /src/models/research/object_detection/model_main_tf2.py --pipeline_config_path=/content/training/pipeline.config --model_dir=/content/train_dir --alsologtostderr

ELAPSED_TRAIN="Train: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"

python /src/models/research/object_detection/exporter_main_v2.py --input_type image_tensor --pipeline_config_path /content/training/pipeline.config --trained_checkpoint_dir /content/train_dir --output_directory /content/saved_model_dir

python /src/models/research/object_detection/export_tflite_graph_tf2.py --pipeline_config_path /content/saved_model_dir/pipeline.config --trained_checkpoint_dir /content/saved_model_dir/checkpoint --output_directory /content/tflite
tflite_convert --saved_model_dir=/content/tflite/saved_model --output_file=/content/tflite/model.tflite

echo $ELAPSED_PREP
echo $ELAPSED_TRAIN

echo "EXPECTED:"
echo "SAVED MODEL:"
echo "/content/saved_model_dir:"
echo "pipeline.config  saved_model  checkpoint"

echo "/content/saved_model_dir/saved_model:"
echo "saved_model.pb  fingerprint.pb  assets  variables"

echo "/content/saved_model_dir/saved_model/assets:"

echo "/content/saved_model_dir/saved_model/variables:"
echo "variables.data-00000-of-00001  variables.index"

echo "/content/saved_model_dir/checkpoint:"
echo "checkpoint  ckpt-0.index  ckpt-0.data-00000-of-00001"
echo "EXPORT:"
echo "/content/tflite/model.tflite"
echo ""
echo "ACTUAL:"
echo "SAVED MODEL:"
ls -tR /content/saved_model_dir
echo "EXPORT:"
ls -tR /content/tflite/*.*

python /src/model_evaluator.py --model_path /content/tflite/model.tflite --test_samples /content/training/test --target_width 320 --target_height 320