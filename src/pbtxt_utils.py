def get_pbtxt_item(class_name, i):
    return f"""item {{
id: {i}
name: '{class_name}'
}}"""

def get_pbtxt(classes):
    return "\n".join([get_pbtxt_item(class_name, i+1) for i, class_name in enumerate(classes)])