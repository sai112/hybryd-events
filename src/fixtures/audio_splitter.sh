#!/bin/bash

window=10
step=4

for f in $input_dir/*.wav; do
    mkdir $output_dir/$(basename ${f/.wav})
    seconds_f=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 $f)
    seconds=${seconds_f%.*}
    parts=$(( seconds / step ))
    last_part=$(( parts - 5))
    for i in $(seq $last_part); do
        #start=$(date -d@$(( i * step )) -u +%M:%S)
        #end=$(date -d@$(( i * step + step )) -u +%M:%S)
        echo "==================================================="
        echo "==================================================="
        echo $(( i * step ))
        echo $f
        echo $window
        echo $output_dir/$(basename ${f/.wav})/$(basename ${f/.wav})_$i.wav
        echo "==================================================="
        ffmpeg -ss $(( i * step )) -i $f -t $window -acodec copy $output_dir/$(basename ${f/.wav})/$(basename ${f/.wav})_$i.wav
    done
done