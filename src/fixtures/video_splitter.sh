#!/bin/bash

window=10
step=4

for f in $input_dir/*.mp4; do
    mkdir $tmp_dir/$(basename ${f/.mp4})
    seconds_f=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 $f)
    seconds=${seconds_f%.*}
    parts=$(( seconds / step ))
    last_part=$(( parts - 5))
    for i in $(seq $last_part); do
        start=$(date -d@$(( i * step )) -u +%M:%S)
        end=$(date -d@$(( i * step + step )) -u +%M:%S)
        ffmpeg -ss $start:00 -i $f -t $window -acodec copy -vcodec copy $tmp_dir/$(basename ${f/.mp4})/$(basename ${f/.mp4})_$i.mp4
    done
done

for d in $tmp_dir/*; do
    mkdir $output_dir/$(basename $d)
    for f in $d/*.mp4; do
        #ffmpeg -i $f -frames:v 1 $output_dir/$(basename $d)/$(basename ${f/.mp4}).jpg
        ffmpeg -i $f -vn -acodec copy $output_dir/$(basename $d)/$(basename ${f/.mp4}).wav
    done
done