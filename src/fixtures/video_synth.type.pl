use File::Basename;
use File::Copy;
use File::Spec::Functions;

my $source_img_type1 = $ARGV[0];
my $source_audio_type1 = $ARGV[1];
my $source_img_type2 = $ARGV[2];
my $source_audio_type2 = $ARGV[3];
my $target_dir = $ARGV[4];
my $count = $ARGV[5];
my $class_name_1 = $ARGV[6];
my $class_name_2 = $ARGV[7];
my $out_dir = $ARGV[8];
my $out_dir_separate_audio = $ARGV[9];

my @source_img_type1_files = glob($source_img_type1.'/*.jpg');
my @source_audio_type1_files = glob($source_audio_type1.'/*.wav');
my @source_img_type2_files = glob($source_img_type2.'/*.jpg');
my @source_audio_type2_files = glob($source_audio_type2.'/*.wav');

sub create_video_map 
{
    my @img_files = glob($_[0].'/*.jpg');
    my @audio_files = glob($_[1].'/*.wav');
    my $class_name = $_[2];
    my $idx = $_[3];
    my $target_d = $_[4];
    my $out_d = $_[5];
    my $out_dir_sa = $_[6];

    my $randf_i = $img_files[rand @img_files];
    my $randf_a = $audio_files[rand @audio_files];    
    my $new_dir = catfile($target_d, $class_name.$idx);
    
    mkdir $new_dir;
    copy($randf_i, catfile($new_dir, "image.jpg"));
    copy($randf_a, catfile($new_dir, "audio.wav"));

    open(my $out, ">", catfile($new_dir, "list_a.txt")) or die "File couldn't be opened";
    for (1 .. 12) {
        print $out "file 'audio.wav'\n";
    }
    close $out or "couldn't close";

    system("ffmpeg", "-f", "concat", "-safe", "0", "-i", catfile($new_dir, "list_a.txt"), "-c", "copy", catfile($new_dir, "audio_combined.wav"));
    system("ffmpeg", "-i", catfile($new_dir, "audio_combined.wav"), "-af", "volume=10", catfile($new_dir, "audio_combined_filt.wav"));
    system("ffmpeg", "-loop", "1", "-i", catfile($new_dir, "image.jpg"), "-c:v", "libx264", "-t", "120", "-pix_fmt", "yuv420p", catfile($new_dir, "images.mp4"));
    #system("ffmpeg", "-framerate", "1", "-loop", "1", "-i", catfile($new_dir, "image.jpg"), "-c:v", "libx264", "-x264-params", "keyint=5", "-t", "120", "-vf", "format=yuv420p", "-r", "10", catfile($new_dir, "images.mp4"));
    #-x264-params keyint=20
    #ffmpeg -framerate 1 -loop 1 -i image.jpg -i audio.mp3 -vf format=yuv420p -r 10 -shortest -movflags +faststart output.mp4
    copy(catfile($new_dir, "audio_combined_filt.wav"), catfile($out_dir_sa, $class_name.$idx.".wav"));
    system("ffmpeg", "-i", catfile($new_dir, "images.mp4"), "-i", catfile($new_dir, "audio_combined_filt.wav"), "-c:v", "copy", "-c:a", "copy", catfile($new_dir, $class_name.$idx.".mp4"));

    move(catfile($new_dir, $class_name.$idx.".mp4"), catfile($out_d, $class_name.$idx.".mp4"))
}

for my $i (1 .. $count) {
    if (rand() < 0.5) {
        create_video_map($source_img_type1, $source_audio_type1, $class_name_1, $i, $target_dir, $out_dir, $out_dir_separate_audio);
    } else {
        create_video_map($source_img_type2, $source_audio_type2, $class_name_2, $i, $target_dir, $out_dir, $out_dir_separate_audio);
    }
}