import mnist
import numpy as np
from PIL import Image

mn = mnist.MNIST('/content/data_mnist_raw')
test_img, test_label = mn.load_testing()
train_img, train_label = mn.load_training()

for i, (train_im, train_l) in enumerate(zip(train_img, train_label)):
    imagenp = np.array(train_im, 'uint8').reshape((28, 28))
    image = Image.fromarray(imagenp, 'L')
    image.save(f'/content/data_mnist/train/d{train_l}/d{train_l}_{i}.png')

for i, (test_im, test_l) in enumerate(zip(test_img, test_label)):
    imagenp = np.array(test_im, 'uint8').reshape((28, 28))
    image = Image.fromarray(imagenp, 'L')
    image.save(f'/content/data_mnist/test/d{test_l}/d{test_l}_{i}.png')