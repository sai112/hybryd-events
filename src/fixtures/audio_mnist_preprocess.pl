use File::Basename;
use File::Copy;

$train_percent = $ARGV[0];
$source_dir = $ARGV[1];
$target_dir = $ARGV[2];

$train_percent_norm = $train_percent / 100;

my @files = glob($source_dir.'/*/*.wav');

foreach my $file (@files){
    $file =~ /(\d)_.*/;

    if (rand() < $train_percent_norm) {
        move($file, $target_dir.'/train/d'.$1.'/d'.basename($file)) or die "The move operation failed: $!";
    } else {
        move($file, $target_dir.'/test/d'.$1.'/d'.basename($file)) or die "The move operation failed: $!";
    }
}