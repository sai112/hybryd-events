import matplotlib.pyplot as plt
import cv2
import librosa
import numpy as np

def read_audio(conf, pathname, trim_long_data):
    y, sr = librosa.load(pathname, sr=conf.sampling_rate)
    if len(y) > conf.samples:
        if trim_long_data:
            y = y[0:0+conf.samples]
    else:
        padding = conf.samples - len(y)
        offset = padding // 2
        y = np.pad(y, (offset, conf.samples - len(y) - offset), 'constant')
    return y

def audio_to_melspectrogram(conf, audio):
    spectrogram = librosa.feature.melspectrogram(audio,
                                 sr=conf.sampling_rate,
                                 n_mels=conf.n_mels,
                                 hop_length=conf.hop_length,
                                 n_fft=conf.n_fft,
                                 fmin=conf.fmin,
                                 fmax=conf.fmax,
                                 )
    spectrogram = librosa.power_to_db(spectrogram)
    spectrogram = spectrogram.astype(np.float32)
    return spectrogram

def audio_to_spectrogram(wav):
    D = librosa.stft(wav, n_fft=256, hop_length=128, win_length=256, window='hamming')
    spect, phase = librosa.magphase(D)
    spect = spect.astype(np.float32)
    return spect   

def read_as_melspectrogram(conf, pathname, trim_long_data, debug_display=False):
    x = read_audio(conf, pathname, trim_long_data)
    mels = audio_to_melspectrogram(conf, x)
    return mels

class conf:
    sampling_rate = 16000
    duration = 10
    hop_length = 125*duration
    fmin = 200
    fmax = sampling_rate // 2
    n_mels = 128
    n_fft = n_mels * 20
    samples = sampling_rate * duration

def mono_to_color(X, mean=None, std=None, norm_max=None, norm_min=None, eps=1e-6):
    X = np.stack([X, X, X], axis=-1)

    mean = mean or X.mean()
    std = std or X.std()
    Xstd = (X - mean) / (std + eps)
    _min, _max = Xstd.min(), Xstd.max()
    norm_max = norm_max or _max
    norm_min = norm_min or _min
    if (_max - _min) > eps:
        V = Xstd
        V[V < norm_min] = norm_min
        V[V > norm_max] = norm_max
        V = 255 * (V - norm_min) / (norm_max - norm_min)
        V = V.astype(np.uint8)
    else:
        V = np.zeros_like(Xstd, dtype=np.uint8)
    return V

def rename_file(img_name):
    img_name = img_name.split("/")[-1]
    img_name = img_name[:-4]
    img_name += ".jpg"
    return img_name

def save_image_from_sound(img_path, save_path):
    x = read_as_melspectrogram(conf, img_path, trim_long_data=False, debug_display=True)
    
    widthHeight=(5.14, 5.14)
    plt.figure(figsize=widthHeight)
    fig = plt.imshow(x, interpolation='nearest')
    plt.axis('off')
    plt.savefig(save_path, bbox_inches='tight', pad_inches = 0)
    plt.close()

def save_image_from_sound1(img_path, save_path, trim_long_data=False):
    audio = read_audio(conf, img_path, trim_long_data)
    x = audio_to_spectrogram(audio)
    log_spect = np.log(x)

    widthHeight=(5.14, 5.14)
    plt.figure(figsize=widthHeight)
    plt.imshow(log_spect, aspect='auto', origin='lower', cmap='inferno')
    plt.axis('off')
    plt.savefig(save_path, bbox_inches='tight', pad_inches = 0)
    plt.close()

#def resize(img_path, width, height):
#    img = cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
#    img_resized = cv2.resize(img, (width, height))
#    cv2.imwrite(img_path, img_resized) 