import os
import shutil
import argparse
from pathlib import Path
import glob
from PIL import Image

parser = argparse.ArgumentParser(description='...')
parser.add_argument('--source', type=str)
parser.add_argument('--target', type=str)
parser.add_argument('--dimensions', type=int)
parser.add_argument('--width', type=int)
parser.add_argument('--height', type=int)
args = parser.parse_args()

def ignore_files(dir, files):
    return [f for f in files if os.path.isfile(os.path.join(dir, f))]

def main():
    if args.width and args.height:
        width = args.width
        height = args.height
    elif args.dimensions:
        width = args.dimensions
        height = args.dimensions
    else:
        raise ValueError("Missing dimensions arguments")

    source_dir_root = Path(args.source)
    target_dir_root = Path(args.target)
    if not source_dir_root.exists() or not source_dir_root.is_dir():
        raise ValueError("Source does not exist")

    shutil.copytree(args.source, args.target, ignore=ignore_files, dirs_exist_ok=False)

    for fp in [Path(f) for f in glob.glob(f'{source_dir_root}/**/*.*', recursive=True)]:
        with Image.open(fp) as im:
            #im.resize((width, height), Image.Resampling.NEAREST).save(target_dir_root / fp.relative_to(source_dir_root))
            im.resize((width, height), Image.Resampling.NEAREST).save(target_dir_root / fp.relative_to(source_dir_root).with_suffix(".jpg"), format="jpeg")

if __name__ == "__main__":
    main()