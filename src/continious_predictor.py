#first attempt - worked with cv2
import os
import numpy as np
import tensorflow as tf
import cv2
import time
import random
import file_selector as fsel

IMG_WIDTH = 320
IMG_HEIGHT = 320

interpreter = tf.lite.Interpreter(model_path="/content/tflite/model.tflite")
interpreter.allocate_tensors()

input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

def predict(file_name):
    img_BGR = cv2.imread(str(fsel.get_file_from_training(file_name[:-4]+".png", "test")))
    img = cv2.cvtColor(img_BGR, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img,(IMG_WIDTH,IMG_HEIGHT))#Preprocess the image to required size and cast
    img_for_output = img.copy()
    img = (img - 127.5) / 127.5
    input_shape = input_details[0]['shape']
    input_tensor= np.array(np.expand_dims(img,0), dtype=np.float32)

    input_index = interpreter.get_input_details()[0]["index"]
    interpreter.set_tensor(input_index, input_tensor)#Run the inference
    interpreter.invoke()
    output_details = interpreter.get_output_details()

    detection_boxes = interpreter.get_tensor(output_details[1]['index'])
    detection_classes = interpreter.get_tensor(output_details[3]['index'])
    detection_scores = interpreter.get_tensor(output_details[0]['index'])
    num_boxes = interpreter.get_tensor(output_details[2]['index'])

    probability = detection_scores[0][0]
    dclass = int(detection_classes[0][0])
    print(f"{file_name}: {dclass} / {probability}")

import logging
import sys
import time

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

logging.basicConfig(level=logging.DEBUG)

class MyEventHandler(FileSystemEventHandler):
    def on_created(self, event):
        predict(event.src_path)

path = sys.argv[1]

event_handler = MyEventHandler()
observer = Observer()
observer.schedule(event_handler, path, recursive=True)
observer.start()
try:
    while True:
        time.sleep(1)
finally:
    observer.stop()
    observer.join()