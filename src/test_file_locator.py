import pytest
from pathlib import Path
import file_locator

custom_root = Path("/croot")

class_expected = Path("/croot/data/train/class12")
training_root_expected = Path("/croot/training")
training_expected = Path("/croot/training/test")
file_from_training_expected = Path("/croot/training/test/a")
model_loc_expected = Path("/croot/ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8")
pbtxt_loc_expected = Path("/croot/training/object_detection.pbtxt")
pipeline_loc_expected = Path("/croot/training/pipeline.config")
model_data_labels_loc_expected = Path("/croot/training/train_labels.csv")
model_data_record_loc_expected = Path("/croot/training/test.record")

def test_get_class():
    class_actual = file_locator.get_class("class12", "train", custom_root)
    assert class_actual == class_expected

def test_get_training_root():
    training_root_actual = file_locator.get_training_root(custom_root)
    assert training_root_actual == training_root_expected

def test_get_training():
    training_actual = file_locator.get_training("test", custom_root)
    assert training_actual == training_expected

def test_get_file_from_training():
    file_from_training_actual = file_locator.get_file_from_training("a", "test", custom_root)
    assert file_from_training_actual == file_from_training_expected

def test_get_model_loc():
    model_loc_actual = file_locator.get_model_loc(custom_root)
    assert model_loc_actual == model_loc_expected

def test_get_pbtxt_loc():
    pbtxt_loc_actual = file_locator.get_pbtxt_loc(custom_root)
    assert pbtxt_loc_actual == pbtxt_loc_expected

def test_get_pipeline_loc():
    pipeline_loc_actual = file_locator.get_pipeline_loc(custom_root)
    assert pipeline_loc_actual == pipeline_loc_expected

def test_get_model_data_labels_loc():
    model_data_labels_loc_actual = file_locator.get_model_data_labels_loc("train", custom_root)
    assert model_data_labels_loc_actual == model_data_labels_loc_expected

def test_get_model_data_record_loc():
    model_data_record_loc_actual = file_locator.get_model_data_record_loc("test", custom_root)
    assert model_data_record_loc_actual == model_data_record_loc_expected
