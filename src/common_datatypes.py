from typing import NamedTuple

class InferenceScore(NamedTuple):
    detection_boxes: list
    detection_classes: list
    detection_labels: list
    detection_scores: list
    num_boxes: int

class InferenceResult(NamedTuple):
    inference_score: InferenceScore
    sample: str

def get_inference_result_short_str(ir):
    return f"File: {ir.sample}\tPredicted: {ir.inference_score.detection_classes[0]} ({ir.inference_score.detection_labels[0]})\tScore: {ir.inference_score.detection_scores[0]}"