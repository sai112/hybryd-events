from dictionary import get_noise_descriptions
from pdf_reader import get_text_from_pdf
import argparse


def find_noise(pdf_path):
    text = get_text_from_pdf(pdf_path)
    dictionary = get_noise_descriptions()
    results = list(set(text) & set(dictionary))
    if results:
        print(f'The manual contained the following words about noise {results}')
    else:
        "Noise wasn't found"



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--Path", help="manual path")
    args = parser.parse_args()

    if args.Path:
        find_noise(args.Path)
    else:
        "Run with manual path argument"

