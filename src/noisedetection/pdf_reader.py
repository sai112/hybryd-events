from PyPDF2 import PdfReader
import re
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize

def get_text_from_pdf(pdf_path):
    reader = PdfReader(pdf_path)
    text = ""
    for page in reader.pages:
        text += page.extract_text() + "\n"
    return preproccess(text)

def preproccess(text):
    clean_text = re.sub(r'[^\w\s]', '', re.sub('\n*_+\n*', '', re.sub('\s*\n\s+', '\n',  text)))
    tokens = tokenization(clean_text)
    return lemmatization(tokens)

def tokenization(text):
    functors_pos = {'CC', 'VBZ', 'DT', 'TO', 'CD', 'RB', 'IN', 'PRP', 'VBP', 'WDT', 'MD', 'PRP$'}
    tokens = word_tokenize(text)
    return [word.lower() for word, pos in nltk.pos_tag(tokens, lang='eng') if pos not in functors_pos]

def lemmatization(tokens):
    lemmatizer = WordNetLemmatizer()
    return [lemmatizer.lemmatize(token) for token in tokens]
