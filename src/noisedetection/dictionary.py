from itertools import chain
from nltk.corpus import wordnet

def get_noise_descriptions():
    base_dictionary = [
        'rattling',
        'squealing',
        'buzzing',
        'noise',
        'scraping',
        'abnormal',
        'backfire',
        'banging',
        'chirping',
        'clanging',
        'clanking',
        'clicking',
        'clunking',
        'flapping',
        'grinding',
        'hesitation',
        'hissing',
        'howling',
        'moaning',
        'pinging',
        'popping',
        'roaring',
        'rumbling',
        'screeching',
        'tapping',
        'whining',
        'whistling',
        'knocking'
    ]
    synonyms = [wordnet.synsets(word) for word in base_dictionary]
    for synonym in synonyms:
        for s in synonym:
            if s.lemma_names():
                base_dictionary.append(s.lemma_names()[0])
                if len(s.lemma_names()) >= 2:
                    base_dictionary.append(s.lemma_names()[1])
    return list(set(base_dictionary))
