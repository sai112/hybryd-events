import argparse
import os


def newest_file(path):
    files = os.listdir(path)
    paths = [os.path.join(path, basename) for basename in files]
    return max(paths, key=os.path.getctime)


def generate_answer(device_name, time, place):
    return f'В работе устройства {device_name}' \
           f' замечено {time} аномальное поведение' \
           f' в {place} работы'


def get_places(conditions):
    abnormal_indices = [i for i, x in enumerate(conditions) if x == "1"]
    places = []
    for index in abnormal_indices:
        if index <= (len(conditions) / 3)-1:
            places.append("началe")
        elif (index <= (len(conditions) / 3) * 2 - 1) and index > (len(conditions) / 3) - 1:
            places.append("середине")
        elif (index > (len(conditions) / 3) * 2 - 1) and index <= len(conditions) - 1:
            places.append("конце")
    places = list(set(places))
    return " и ".join(places) if len(places) > 1 else places[0]


def get_time(conditions):
    abnormal_indices = [i for i, x in enumerate(conditions) if x == "1"]
    counts = []
    count = 1
    for i in range(len(abnormal_indices) - 1):
        if abnormal_indices[i+1] - abnormal_indices[i] == 1:
            count += 1
        else:
            counts.append(count)
            count = 1
    if not counts:
        counts.append(count)
    answer = ["краткосрочное" if x < 2 else "долгосрочное" for x in list(set(counts))]
    return " и ".join(answer) if len(answer) > 1 else answer[0]


def parse_file(file_path):
    device_name = os.path.basename(file_path).split('.')[0]
    with open(file_path, 'r') as f:
        content = f.read()

    conditions = [row for row in content if row != '\n']
    places = get_places(conditions)
    time = get_time(conditions)
    print(generate_answer(device_name, time, places))


def parse_array(d_name, mode):
    device_name = d_name
    conditions = mode
    places = get_places(conditions)
    time = get_time(conditions)
    print(generate_answer(device_name, time, places))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--Directory", help="Directory path")
    parser.add_argument('--device', type=str)
    parser.add_argument('--mode', type=str, nargs='+')
    args = parser.parse_args()

    if args.Directory:
        parse_file(newest_file(args.Directory))
    elif args.mode and args.device:
        parse_array(args.device, args.mode)
    else:
        "Run with directory path argument"
