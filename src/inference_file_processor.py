import os
from pathlib import Path
import numpy as np
import cv2
import inference_processor
import spectrogram_utils
import common_datatypes
import argparse

def read_image(file_path, width, height):
    img_BGR = cv2.imread(str(file_path))
    img = cv2.cvtColor(img_BGR, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img, (width, height))
    img_for_output = img.copy()
    img = (img - 127.5) / 127.5
    return img

def predict_file(InferenceProcessor, file_path, width=320, height=320, intermediate_storage=None):
    if file_path.suffix == ".jpg":
        img = read_image(file_path, width, height)
        inference_scores = InferenceProcessor.predict(img)
        return common_datatypes.InferenceResult(inference_scores, str(file_path))
    elif file_path.suffix == ".wav":
        if intermediate_storage is None:
            raise ValueError("Processing audio files requires specifying intermediate storage.")
        intermediate_path = intermediate_storage / file_path.with_suffix(".jpg").name
        spectrogram_utils.save_image_from_sound1(str(file_path), str(intermediate_path))
        img = read_image(intermediate_path, width, height)
        inference_scores = InferenceProcessor.predict(img)
        return common_datatypes.InferenceResult(inference_scores, str(file_path))
    else:
        raise ValueError(f"Wrong file type: {file_path.suffix}. Only JPEG and WAV are supported.")

def predict(InferenceProcessor, files, width=320, height=320, intermediate_storage=None):
    if type(files) is list:
        return list([predict_file(InferenceProcessor, f, width, height, intermediate_storage) for f in files])
    elif files.is_file():
        return predict_file(InferenceProcessor, files, width, height, intermediate_storage)
    elif files.is_dir():
        return predict(InferenceProcessor, list([files / Path(f) for f in os.listdir(str(files))]), width, height, intermediate_storage)
    else:
        raise ValueError(f"Unrecognized source: {files}. Must be a list of files, a file, or a dir")

def main(args):
    ip = inference_processor.InferenceProcessor(args.model_path)
    if args.target_width and args.target_height:
        irs = predict(ip, Path(args.source_dir), args.target_width, args.target_height, Path(args.intermediate_storage))
    else:
        irs = predict(ip, Path(args.source_dir), intermediate_storage=Path(args.intermediate_storage))

    for ir in irs:
        print(common_datatypes.get_inference_result_short_str(ir))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='...')
    parser.add_argument('--model_path', type=str, default="/content/tflite/model.tflite")
    parser.add_argument('--source_dir', type=str, default="/content/data")
    parser.add_argument('--target_width', type=int, default=320)
    parser.add_argument('--target_height', type=int, default=320)
    parser.add_argument('--intermediate_storage', type=str, default="/content/intermediate")
    args = parser.parse_args()
    main(args)