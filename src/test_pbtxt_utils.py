import pytest
import pbtxt_utils

pbtxt_item_expected = f"""item {{
id: 4
name: 'class15'
}}"""

def test_get_pbtxt_item():
    pbtxt_item_actual = pbtxt_utils.get_pbtxt_item("class15", 4)
    assert pbtxt_item_actual == pbtxt_item_expected

pbtxt_expected = f"""item {{
id: 1
name: 'class15'
}}
item {{
id: 2
name: 'class165'
}}"""

def test_get_pbtxt():
    pbtxt_actual = pbtxt_utils.get_pbtxt(["class15", "class165"])
    assert pbtxt_actual == pbtxt_expected