import os
import numpy as np
import tensorflow as tf
import cv2
from object_detection.utils import label_map_util
import file_locator as floc
from pathlib import Path
from pycm import *
import argparse
from pathlib import Path
import inference_processor
import inference_file_processor
import common_datatypes

DEFAULT_DIMENSIONS = 320

def main(args):
    ip = inference_processor.InferenceProcessor(args.model_path, args.label_map)

    label_map = label_map_util.load_labelmap(args.label_map)
    label_map_dict = label_map_util.get_label_map_dict(label_map)

    width = DEFAULT_DIMENSIONS
    height = DEFAULT_DIMENSIONS
    if args.target_width and args.target_height:
        width = args.target_width
        height = args.target_height
    elif args.dimensions:
        width = args.dimensions
        height = args.dimensions

    num_files = 0

    gt = []
    pred = []
    pred_lbl = []

    for file_name in [Path(args.test_samples) / fn for fn in os.listdir(args.test_samples) if Path(fn).suffix == '.jpg']:
        #gt.append(int(label_map_dict[file_name.name[:2]]) - 1)
        gt.append(int(label_map_dict[file_name.name.split("_")[0]]) - 1)
        
        ir = inference_file_processor.predict_file(ip, file_name, width, height)
        print(common_datatypes.get_inference_result_short_str(ir))

        pred.append(int(ir.inference_score.detection_classes[0]))
        pred_lbl.append(ir.inference_score.detection_labels[0])

        num_files = num_files + 1
        if args.max_samples:
            if num_files > args.max_samples:
                break

    cm = ConfusionMatrix(actual_vector=gt, predict_vector=pred)
    cm.print_matrix()
    cm.print_normalized_matrix()
    cm.stat(summary=True)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='...')
    parser.add_argument('--model_path', type=str, default="/content/tflite/model.tflite")
    parser.add_argument('--label_map', type=str, default="/content/training/object_detection.pbtxt")
    parser.add_argument('--test_samples', type=str, default="/content/training/test")
    parser.add_argument('--target_dimensions', type=int)
    parser.add_argument('--target_width', type=int)
    parser.add_argument('--target_height', type=int)
    parser.add_argument('--max_samples', type=int)
    args = parser.parse_args()

    main(args)