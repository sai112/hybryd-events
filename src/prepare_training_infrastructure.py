import tensorflow as tf
from shutil import copyfile
import random
import argparse
from pathlib import Path
import file_locator as floc
import pbtxt_utils
import xml_config_utils
import generate_tfrecord
import pipeline_utils

TRAIN_TEST = ['train', 'test']

n_samples = 5000000

def main(args):
    target_classes = args.classes

    CR = Path(args.content_root)

    with open(floc.get_pbtxt_loc(root=CR), "w") as file:
        file.write(pbtxt_utils.get_pbtxt(target_classes))

    def write_xml_detection_file(file_name, class_name):
        with open(file_name.with_suffix(".xml"), "w") as file:
            file.write(xml_config_utils.get_xml_detection(file_name, class_name, 320, 320))

    for train_test in TRAIN_TEST:
        for target_class in target_classes:
            img_files = floc.get_files_from_class(target_class, train_test, root=CR)
            random.shuffle(img_files)
            print(f"target_class ({train_test}): {target_class} / files: {len(img_files)}")
            for file_name in img_files:
                new_fn = floc.get_file_from_training(file_name.name, train_test, root=CR)
                copyfile(file_name, new_fn)
                write_xml_detection_file(new_fn, target_class)

    for train_test in TRAIN_TEST:
        generate_tfrecord.create_tf_record(
            str(floc.get_model_data_record_loc(train_test, root=CR)), 
            floc.get_training(train_test, root=CR),
            floc.get_model_data_labels_loc(train_test, root=CR),
            floc.get_pbtxt_loc(root=CR))#,
            #image_ext="png")

    if args.model:
        if args.model == "mobilenetv2":
            pipeline_utils.create_pipeline_mobilenetv2(floc.get_model_loc_custom(args.model, root=CR), floc.get_training_root(root=CR), len(target_classes), args.batch, args.steps)
        elif args.model == "resnet50v1":
            pipeline_utils.create_pipeline_resnet50v1(floc.get_model_loc_custom(args.model, root=CR), floc.get_training_root(root=CR), len(target_classes), args.batch, args.steps)
    else:
        pipeline_utils.create_pipeline_mobilenetv2(floc.get_model_loc(root=CR), floc.get_training_root(root=CR), len(target_classes), args.batch, args.steps)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='...')
    parser.add_argument('--content_root', type=str, default="/content")
    parser.add_argument('--classes', type=str, nargs='+')
    parser.add_argument('--model', type=str, default="mobilenetv2")
    parser.add_argument('--batch', type=int, default=10)
    parser.add_argument('--steps', type=int, default=2000)
    args = parser.parse_args()

    main(args)