import random
from audiomentations import Compose, AddGaussianNoise, AddBackgroundNoise, TimeStretch, PitchShift, Shift
import soundfile
import librosa

augment_stretch = Compose([
    #AddGaussianNoise(min_amplitude=0.001, max_amplitude=0.015, p=0.5),
    #AddBackgroundNoise(sounds_path='/content/dcase2022_task2/test_cut/engine2_broken', noise_rms="absolute", \
    #                   min_absolute_rms_in_db=-65, max_absolute_rms_in_db=-40, p=1.0),
    TimeStretch(min_rate=0.8, max_rate=1.0, leave_length_unchanged=True, p=1.0),
    #PitchShift(min_semitones=-7, max_semitones=7, p=0.5),
    #Shift(min_fraction=-0.2, max_fraction=0.2, fade=True, fade_duration=0.01, p=0.5),
])

augment_pitchshift = Compose([
    #AddGaussianNoise(min_amplitude=0.001, max_amplitude=0.015, p=0.5),
    #AddBackgroundNoise(sounds_path='/content/dcase2022_task2/test_cut/engine2_broken', noise_rms="absolute", \
    #                   min_absolute_rms_in_db=-65, max_absolute_rms_in_db=-40, p=1.0),
    #TimeStretch(min_rate=0.8, max_rate=1.25, leave_length_unchanged=True, p=0.9),
    PitchShift(min_semitones=0, max_semitones=12, p=1.0),
    #Shift(min_fraction=-0.2, max_fraction=0.2, fade=True, fade_duration=0.01, p=0.5),
])

def augment_audio(file_path_source, file_path_output, augmentation_type="stretch"):
    sound_file, sr = librosa.load(file_path_source, sr=None)
    if augmentation_type=="stretch":
        soundfile.write(file_path_output, augment_stretch(samples=sound_file, sample_rate=sr), 16000, subtype="PCM_16")
    elif augmentation_type=="pitchshift":
        soundfile.write(file_path_output, augment_pitchshift(samples=sound_file, sample_rate=sr), 16000, subtype="PCM_16")
    else:
        raise ValueError(f"Unrecognized augmentation type: {augmentation_type}")