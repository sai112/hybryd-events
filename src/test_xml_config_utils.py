import pytest
from pathlib import Path
import xml.etree.ElementTree as ET

import xml_config_utils

annotation_expected = f"""<annotation>
  <folder>images</folder>
  <filename>b</filename>
  <path>/a/b</path>
  <source>
    <database>Unknown</database>
  </source>
  <size>
    <width>32</width>
    <height>32</height>
    <depth>3</depth>
  </size>
  <segmented>0</segmented>
  <object>
    <name>class1</name>
    <pose>Unspecified</pose>
    <truncated>0</truncated>
    <difficult>0</difficult>
    <bndbox>
      <xmin>0</xmin>
      <ymin>0</ymin>
      <xmax>32</xmax>
      <ymax>32</ymax>
    </bndbox>
   </object>
</annotation>"""

def test_create_xml_detection():
    annotation_actual = xml_config_utils.get_xml_detection(Path("/a/b"), "class1", 32, 32)
    assert annotation_actual == annotation_expected

csv_records_expected = [("b", 32, 32, "class1", 0, 0, 32, 32)]

def test_xml_record_to_df_records():
    csv_records_actual = xml_config_utils.xml_record_to_df_records(ET.fromstring(annotation_expected))
    assert csv_records_actual == csv_records_expected