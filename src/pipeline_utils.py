import re

def create_pipeline_mobilenetv2(model_root, data, num_classes, batch_size, num_steps):
    filename = model_root / "pipeline.config"
    pipeline = data / "pipeline.config"

    with open(filename) as f:
        s = f.read()
    with open(pipeline, 'w') as f:
        f.write(transform_pipeline_mobilenetv2(s, model_root, data, num_classes, batch_size, num_steps))

def transform_pipeline_mobilenetv2(pipeline_text, model_root, data, num_classes, batch_size, num_steps):
    ckpt = model_root / "checkpoint" / "ckpt-0"
    pbtxt = data / "object_detection.pbtxt"
    test_record = data / "test.record"
    train_record = data / "train.record"

    result = pipeline_text

    result = re.sub('num_classes: 90', f'num_classes: {num_classes}', result)
    result = re.sub('batch_size: 128', f'batch_size: {batch_size}', result)
    result = re.sub('fine_tune_checkpoint: "PATH_TO_BE_CONFIGURED"', f'fine_tune_checkpoint: "{ckpt}"', result)
    result = re.sub('num_steps: 50000', f'num_steps: {num_steps}', result)
    result = re.sub('fine_tune_checkpoint_type: "classification"', 'fine_tune_checkpoint_type: "detection"', result)
    result = re.sub('label_map_path: "PATH_TO_BE_CONFIGURED"', f'label_map_path: "{pbtxt}"', result)
    result = re.sub('input_path: "PATH_TO_BE_CONFIGURED"', f'input_path: "{test_record}"', result)
    result = re.sub('input_path: ".*record"', f'input_path: "{train_record}"', result, 1)
    #result = re.sub('input_path: "PATH_TO_BE_CONFIGURED"', f'input_path: "{train_record}"', result, 1)

    return result

def create_pipeline_resnet50v1(model_root, data, num_classes, batch_size, num_steps):
    filename = model_root / "pipeline.config"
    pipeline = data / "pipeline.config"

    with open(filename) as f:
        s = f.read()
    with open(pipeline, 'w') as f:
        f.write(transform_pipeline_resnet50v1(s, model_root, data, num_classes, batch_size, num_steps))

def transform_pipeline_resnet50v1(pipeline_text, model_root, data, num_classes, batch_size, num_steps):
    ckpt = model_root / "checkpoint" / "ckpt-0"
    pbtxt = data / "object_detection.pbtxt"
    test_record = data / "test.record"
    train_record = data / "train.record"

    result = pipeline_text

    result = re.sub('num_classes: 90', f'num_classes: {num_classes}', result)
    result = re.sub('batch_size: 64', f'batch_size: {batch_size}', result)
    result = re.sub('fine_tune_checkpoint: "PATH_TO_BE_CONFIGURED"', f'fine_tune_checkpoint: "{ckpt}"', result)
    result = re.sub('total_steps: 25000', f'total_steps: {num_steps}', result)
    result = re.sub('num_steps: 25000', f'num_steps: {num_steps}', result)
    #result = re.sub('max_number_of_boxes: 100', f'max_number_of_boxes: 10', result)
    result = re.sub('fine_tune_checkpoint_type: "classification"', 'fine_tune_checkpoint_type: "detection"', result)
    result = re.sub('label_map_path: "PATH_TO_BE_CONFIGURED"', f'label_map_path: "{pbtxt}"', result)
    result = re.sub('input_path: "PATH_TO_BE_CONFIGURED"', f'input_path: "{test_record}"', result)
    result = re.sub('input_path: ".*record"', f'input_path: "{train_record}"', result, 1)
    #result = re.sub('input_path: "PATH_TO_BE_CONFIGURED"', f'input_path: "{train_record}"', result, 1)

    return result