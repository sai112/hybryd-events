import numpy as np
import tensorflow as tf
from object_detection.utils import label_map_util
from common_datatypes import InferenceScore

def reverse_dict(old_dict):
    new_dict = {}
    for key, value in old_dict.items():
        new_dict[value]=key
    return new_dict

class InferenceProcessor:
    def __init__(self, model_path, pbtxt_loc=None):
        if pbtxt_loc is not None:
            label_map = label_map_util.load_labelmap(pbtxt_loc)
            label_map_dict = label_map_util.get_label_map_dict(label_map)
            self.label_map_dict_inv = reverse_dict(label_map_dict)
            print(label_map_dict)
            print(self.label_map_dict_inv)

        self.interpreter = tf.lite.Interpreter(model_path)
        self.interpreter.allocate_tensors()
        self.input_index = self.interpreter.get_input_details()[0]["index"]
    
    def predict_sample(self, sample):
        input_tensor = np.array(np.expand_dims(sample, 0), dtype=np.float32)        
        self.interpreter.set_tensor(self.input_index, input_tensor)
        self.interpreter.invoke()
        output_details = self.interpreter.get_output_details()

        detection_boxes = self.interpreter.get_tensor(output_details[1]['index'])
        detection_classes = self.interpreter.get_tensor(output_details[3]['index'])
        detection_scores = self.interpreter.get_tensor(output_details[0]['index'])
        num_boxes = self.interpreter.get_tensor(output_details[2]['index'])

        #arr_probability.append(detection_scores[0][0])
        #arr_class.append(int(detection_classes[0][0]))
        #pred.append(int(detection_classes[0][0]))
        #arr_file.append(file_name)

        detection_labels = list([self.label_map_dict_inv[int(c) + 1] for c in detection_classes[0]]) if self.label_map_dict_inv else None

        return InferenceScore(detection_boxes[0], detection_classes[0], detection_labels, detection_scores[0], num_boxes)
    
    def predict(self, samples):
        if type(samples) is list:
            return list([self.predict_sample(sample) for sample in samples])
        return self.predict_sample(samples)