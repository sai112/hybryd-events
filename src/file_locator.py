import os
from pathlib import Path

DefaultRoot = Path("/content")

def get_class(class_name, train_test, root=DefaultRoot):
    if root:
        return root / "data" / train_test / class_name
    else:
        return DefaultRoot / "data" / train_test / class_name

def get_training_root(root=DefaultRoot):
    return root / "training"

def get_training(train_test, root=DefaultRoot):
    return get_training_root(root) / train_test

def get_file_from_training(fn, train_test, root=DefaultRoot):
    return get_training(train_test, root) / fn

def get_files_from_class(class_name, train_test, filter="", root=DefaultRoot):
    from_class = get_class(class_name, train_test, root)
    return [from_class / f for f in os.listdir(from_class) if not filter or f.find(filter)]

def get_model_loc(root=DefaultRoot):
    return root / "ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8"

def get_model_loc_custom(model_name, root=DefaultRoot):
    if model_name == "mobilenetv2":
        return root / "ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8"
    elif model_name == "resnet50v1":
        return root / "ssd_resnet50_v1_fpn_640x640_coco17_tpu-8"
    return root / "ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8"

def get_pbtxt_loc(root=DefaultRoot):
    return get_training_root(root) / "object_detection.pbtxt"

def get_pipeline_loc(root=DefaultRoot):
    return get_training_root(root) / "pipeline.config"

def get_model_data_labels_loc(train_test, root=DefaultRoot):
    return get_training_root(root) / f"{train_test}_labels.csv"

def get_model_data_record_loc(train_test, root=DefaultRoot):
    return get_training_root(root) / f"{train_test}.record"