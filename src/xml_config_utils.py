import os
import glob
from itertools import chain
import pandas as pd
import xml.etree.ElementTree as ET

def get_xml_detection(file_name, class_name, width, height):
    return f"""<annotation>
  <folder>images</folder>
  <filename>{file_name.name}</filename>
  <path>{file_name}</path>
  <source>
    <database>Unknown</database>
  </source>
  <size>
    <width>{width}</width>
    <height>{height}</height>
    <depth>3</depth>
  </size>
  <segmented>0</segmented>
  <object>
    <name>{class_name}</name>
    <pose>Unspecified</pose>
    <truncated>0</truncated>
    <difficult>0</difficult>
    <bndbox>
      <xmin>0.0</xmin>
      <ymin>0.0</ymin>
      <xmax>{width}.0</xmax>
      <ymax>{height}.0</ymax>
    </bndbox>
   </object>
</annotation>"""

def xml_record_to_df_records(el):
    csv_records = []
    for member in el.findall('object'):
        value = (el.find('filename').text,
                 int(el.find('size')[0].text),
                 int(el.find('size')[1].text),
                 member[0].text,
                 int(member[4][0].text[:-2]),
                 int(member[4][1].text[:-2]),
                 int(member[4][2].text[:-2]),
                 int(member[4][3].text[:-2])
                 )
        csv_records.append(value)
    return csv_records

def xml_to_csv(path):
    xml_list = []
    for xml_file in glob.glob(f"{path}/*.xml"):
        xml_list.append(xml_record_to_df_records(ET.parse(xml_file).getroot()))
    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(list(chain(*xml_list)), columns=column_name)
    return xml_df