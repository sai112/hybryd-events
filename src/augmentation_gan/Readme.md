# WaveGAN

WaveGAN code was taken from https://github.com/mostafaelaraby/wavegan-pytorch

## Requirements

```
pip install -r requirements.txt
```

## Augmentation script
To start augmentation install requirements.txt, then you have to paste python acoustic_augmentation.py input/audio/path.wav output/audio/path.wav. Script will apply augmentation to your audio with one of the models from the list in acoustic_augmentation.py
All avaliable models in models/ 
Model working with sr=16000.

