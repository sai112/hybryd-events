import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

if __name__ == "__main__":
    loss_df = pd.read_csv('/home/tema/work/gzp_data/experiment_results/telephone_basic/loss_info.csv')

    plt.plot(smooth(loss_df['generator_loss'].iloc[1:], 3), label="generator")
    plt.plot(smooth(loss_df['discriminator_loss'].iloc[1:], 3), label="discriminator")
    plt.ylim([0, 10])
    plt.legend()
    plt.show()