import os
import soundfile as sf
import numpy as np
import torch

import sys
import subprocess

from models import WaveGANGenerator

path2models = 'models'

window_length = 65536 #[16384, 32768, 65536] in case of a longer window change model_capacity_size to 32
model_capacity_size = 32    # model capacity during training can be reduced to 32 for larger window length of 2 seconds and 4 seconds

noise_latent_dim = 100  # size of the sampling noise

device = "cpu"

models2use = [
    'gan_fan.tar',
    'gan_pump.tar',
    'gan_slider.tar',
    'gan_telephone.tar'
]


def resample(path2input, path2output):
    cmd = [r'ffmpeg', '-i', path2input, '-ac', '1', '-ar', '16000', '-y', path2output]
    subprocess.call(cmd)


def sample_noise(size):
    z = torch.FloatTensor(size, noise_latent_dim).to(device)
    z.data.normal_()  # generating latent space based on normal distribution
    return z

if __name__ == "__main__":
    input_audio_path = sys.argv[1]
    output_audio_path = sys.argv[2]

    generators = []
    # Read models
    for model_name in models2use:
        generator = WaveGANGenerator(slice_len=window_length, model_size=model_capacity_size, use_batch_norm=False, num_channels=1).to("cpu")

        if device == "cuda":
            checkpoint = torch.load(os.path.join(path2models, model_name))
        else:
            checkpoint = torch.load(os.path.join(path2models, model_name), map_location="cpu")
        generator.load_state_dict(checkpoint["generator"])
        generators.append(generator)
    
    # Resample audio
    tmp_audio_path = 'tmp.wav'
    resample(input_audio_path, tmp_audio_path)
    # Read audio should be augmented
    sig, sr = sf.read(tmp_audio_path)
    
    # Select generator randomly from list
    cur_generator = generators[np.random.choice(len(generators))]

    # Apply augmentation
    past_finish = 0
    total_event = []
    while True:
        # Generate acoustic event by random generator from list
        noise = sample_noise(1)
        event = cur_generator(noise)[0, 0]

        total_event.extend(list(event[: sig.shape[0] - past_finish].detach().numpy()))
        # sig[past_finish: past_finish + event.shape[2]] += event
        past_finish += len(event)

        if past_finish >= sig.shape[0]:
            break
    
    total_event = np.array(total_event)
    sig += total_event
    # Dump augmented audio to file
    sf.write(output_audio_path, sig, sr)
    
